# Wokwi - Crosswalk with traffic lights

The project simulates a crosswalk with traffic lights and pedestrian buttons.

The light stays green for the cars until one of the buttons is pressed. When pressed there's a small delay followed by the lights switching to green for the pedestrians. The orange lights by the buttons indicate that the button has been pressed. \
The pedestrian lights blinks green before switching to red. \
The car lights blinks yellow at night.

## Screenshot

![screenshot](screenshot.png)


## Contributors

Eivind Vold Aunebakk
